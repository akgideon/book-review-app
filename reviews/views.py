from django.shortcuts import render, redirect

# Create your views here.
from reviews.models import Review
from reviews.forms import ReviewForm


def create_review(request):
    form = ReviewForm()
    if request.method == "POST":
        form = ReviewForm(request.POST)
        if form.is_valid():
            print("the form is valid")
            form.save()
            return redirect("reviews_list")
    
    
    reviews = Review.objects.all()
    context = {
        "form": form,
    }
    return render(request, "reviews/create.html", context)

def list_reviews(request):
    reviews = Review.objects.all()
    context = {
        "reviews": reviews,
    }
    return render(request, "reviews/main.html", context)